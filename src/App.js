import axios from 'axios';
import { useState } from 'react';
import './App.css';
import { AutoCorrectForm, Picker, List, Button } from './components';



const MINIMUM_CARD_VALUE = 20;
const MAXIMUM_CARD_VALUE = 70;


function App() {



  const [ amountInput, setAmountInput ] = useState();
  const [ combinedCards, setCombinedCards ] = useState([]);
  const [ possibleCards, setPossibleCards] = useState({ upperCard: null, lowerCard: null });

  
  const searchForCombination = async (correctedInput) => {
    setCombinedCards([]);
    correctedInput && setAmountInput(Number(correctedInput));
    const amountToSearch = correctedInput ? correctedInput : amountInput;
    try {
      const res = await axios.get('http://localhost:3001/shop/5/search-combination', {
        headers: {
          'Authorization': `tokenTest123`
        },
        params: {
          amount: amountToSearch,
        }
      })
      
      if(res) {
          res.data.equal && setCombinedCards(res.data.equal.cards);
          setPossibleCards({ upperCard: res?.data?.ceil?.value, lowerCard: res?.data?.floor?.value });
      }
    } catch(err) {
      console.log(err);
    }
  }

  return (
    <div className="App">
      <AutoCorrectForm 
        placeholder='Gift card desired amount'
        type={'number'}
        onChange={setAmountInput}
        onSubmit={searchForCombination}
        value={amountInput}
        autoCorrect = {[
          { 
              condition: amountInput < MINIMUM_CARD_VALUE,
              correction: MINIMUM_CARD_VALUE,
          },
          {
              condition: amountInput > MAXIMUM_CARD_VALUE,
              correction: MAXIMUM_CARD_VALUE,
          }
        ]}
        buttonText='Search'
      />
      { combinedCards.length > 0 && <List items={combinedCards} title={'List of card values to reach amount:'} /> }
      { 
        combinedCards.length === 0 &&
        possibleCards.upperCard !== null &&
        possibleCards.lowerCard !== null && 
        <Picker
          onClick={(event) => { searchForCombination(event.target.innerText) }}
          possibleChoices={[possibleCards.lowerCard, possibleCards.upperCard]}
          text={'We have not found any cards or combination of cards to match your input. Here are alternatives that might interest you:'}
        />
      }

      <Button onClick={() => searchForCombination(possibleCards.lowerCard)} text="minus"/>
      <Button onClick={() => searchForCombination(possibleCards.upperCard)} text="plus" />
    </div>
  );
}

export default App;
