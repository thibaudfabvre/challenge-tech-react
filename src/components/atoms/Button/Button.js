import { SButton } from "./Button.style";




const Button = ({ text, onClick }) => {
    return (
        <SButton onClick={onClick}
        >
            {text}
        </SButton>
    );
}

export default Button;