import { forwardRef } from "react";


const Input = forwardRef(({ type, placeholder, value, onChange }, ref) => {
    return (
        <input 
            ref={ref}
            placeholder={placeholder}
            type={type}
            defaultValue={value} 
            onChange={(e) => {
                onChange(e.target.value);
            }} 
        />
    );
});

export default Input;