import { createRef, useEffect } from "react";
import { Input, Button } from "../../";


const AutoCorrectForm = ({ value, onSubmit, onChange, placeholder, type, buttonText, autoCorrect }) => {
    
    const inputRef = createRef();

    const forceInputUpdate = () => {
        let aConditionWasMet;
        autoCorrect.forEach(({ condition, correction }) => {
            if(condition) {
                aConditionWasMet = true;
                onChange(correction);
                inputRef.current.value = correction;
                onSubmit(correction);

            }
        });
        !aConditionWasMet && onSubmit();
    }

    useEffect(() => {
        inputRef.current.value = value;
    }, [value, inputRef]);


    return(
        <>
            <Input
                ref={inputRef}
                placeholder={placeholder}
                type={type}
                defaultValue={value}
                onChange={onChange} 
            />
            <Button text={buttonText} onClick={autoCorrect ? forceInputUpdate : onSubmit} inputRef={inputRef} />
        </>
    )
}

export default AutoCorrectForm;