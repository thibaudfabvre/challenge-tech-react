import { SListWrapper } from "./List.style";




const List = ({ items, title }) => {
    return(
            <SListWrapper>
                {title}
                {items.map((item, index) => {
                    return(
                    <p key={index}>
                        {item}
                    </p>
                    )
                })}
            </SListWrapper>
    );
};


export default List;