
import { Button } from '../../';

const Picker = ({ possibleChoices, text, onClick}) => {
    return (
          <div>
            <p>{text}</p>
            {
              possibleChoices.map((choice, index) => {
                return(
                  <Button key={index} onClick={onClick} text={choice}/>
                )
              })
            }
          </div>
        )
}

export default Picker;